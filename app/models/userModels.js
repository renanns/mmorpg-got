const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const insertUserDB = async (nome, senha, casa, usuario) => {
    try {
        const user = await prisma.user.create({
            data: {
                name: nome,
                pass: senha,
                user: usuario,
                house: casa
            }
        });
        return user;
    } catch (error) {
        throw error;
    };
};

module.exports = {
    insertUserDB,
};