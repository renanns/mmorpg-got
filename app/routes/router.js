const express = require('express');
const router = express.Router();

const middleware = require('../middleware/middleware')
const controller = require('../controller/controller');

router.get('/', controller.start);

router.get('/register', controller.register);
router.post('/register', middleware.validationUser, controller.insertUser)

router.get('/game', controller.game);

module.exports = router;