const validationUser = (req, res, next) => {
    const {nome, senha, casa, usuario} = req.body;
    const isInvalid = [];

    if(nome == undefined || nome == '') {
        isInvalid.push({msg: 'Nome não pode ser vázio.'});
    };

    if(senha == undefined || senha == '') {
        isInvalid.push({msg: 'Senha não pode ser vázio.'});
    };

    if(casa == undefined || casa == '') {
        isInvalid.push({msg: 'Casa não pode ser vázio.'});
    };

    if (usuario == undefined || usuario == '') {
        isInvalid.push({msg: 'Usuário não pode ser vázio.'});
    };

    if(isInvalid.length > 0) {
        return res.render('register', {isInvalid})
    }

    next();
};

module.exports = {
    validationUser,
}