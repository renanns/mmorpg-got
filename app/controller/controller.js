const models = require('../models/userModels')

const start = (req, res) => {
    return res.render('index');
};

const register = (req, res) => {
    return res.render('register', {isInvalid: []});
};

const insertUser = async (req, res) => {
    const {nome, senha, casa, usuario} = req.body;

    try {
        await models.insertUserDB(nome, senha, casa, usuario);
        res.status(200).json({ message: 'Usuário inserido com sucesso.' });
    } catch (error) {
        res.status(500).json({ message: 'Erro ao inserir usuário.' });
    }
};

const game = (req, res) => {
    return res.render('game');
};


module.exports = {
    start,
    register,
    insertUser,
    game
}